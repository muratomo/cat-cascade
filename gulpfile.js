'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const $ = require("gulp-load-plugins")();

gulp.task('sass', () => {
  const env = (process.env.NODE_ENV === 'prod') ? 'prod' : 'dev';
  return gulp.src('./scss/**/*.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({ outputStyle: 'expanded' }))
    .pipe($.autoprefixer())
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('./css/'));
});

